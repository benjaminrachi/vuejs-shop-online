export default interface Attribute {
  id: string;
  title: string;
  type: string;
  labels: AttributeLabel[];
}

export interface AttributeLabel {
  id: string;
  title: string;
  data: string;
}
