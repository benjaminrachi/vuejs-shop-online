export default interface Cart {
  items: Item[];
  coupon: string;
  total_price: number;
  price_before_discount: number;
  messages: any[];
  pre_order?: any;
}

export interface Image {
  title: string;
  url: string;
}

export interface Variant {
  id: string;
  title: string;
  english_title: string;
  image: Image;
  sku: string;
  price: string;
  discount_price?: any;
  tab_content: any[];
  pre_order_expected_date?: any;
  max_purchase_limit?: any;
  labels: any[];
  metadata: any[];
  recommended_variants: any[];
}

export interface Item {
  id: string;
  product_id: string;
  product_list_price: string;
  product_type: string;
  name: string;
  quantity: number;
  price: number;
  total_price: number;
  variant: Variant;
}


