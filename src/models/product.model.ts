import Variant from './variant.model'
import Attribute from './attribute.model'

export default interface Product {
  id: string;
  title: string;
  description: string;
  images: { title: string, url: string }[];
  min_price: number;
  max_price: number;
  variants: Variant[];
  attributes: Attribute[];
}
