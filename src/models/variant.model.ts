export default interface Variant {
  id: string;
  title: string;
  image: { title: string, url: string };
  labels: VariantLabel[];
  price: number;
}

export interface VariantLabel{
  attribute_id: string;
  label_id: string;
}
