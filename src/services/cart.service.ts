import axios from 'axios'
import Variant from '@/models/variant.model'
import Cart from '@/models/cart.model';

export default new class CartService {
  baseDomain: string = 'https://fedtest.monolith.co.il/api/';
  baseURL: string = `${this.baseDomain}cart`

  add(variant: Variant, qty: number): Promise<{ data: { data: Cart } }> {
    return axios.get(`${this.baseURL}/add?variant__id=${variant.id}&&quantity=${qty}`);
  }
}()
