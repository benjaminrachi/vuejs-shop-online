import axios from 'axios'
import Product from '@/models/product.model'

export default new class CatalogService {
  baseDomain: string = 'https://fedtest.monolith.co.il/api/';
  baseURL: string = `${this.baseDomain}catalog`

  getAllProducts(): Promise<{ data: { data: Product[] } }> {
    return axios.get(`${this.baseURL}/getAll`);
  }

  getProductById(id: string): Promise<{ data: { data: Product } }> {
    return axios.get(`${this.baseURL}/get?id=${id}`);
  }
}()
